#include "iarpa_janus.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
using namespace std;

janus_error janus_load_media(const string &filename, janus_media &media)
{
    Mat img = imread(filename);
    if (!img.data) { // Couldn't load as an image maybe it's a video
        VideoCapture video(filename, cv::CAP_FFMPEG);
        if (!video.isOpened()) {
            fprintf(stderr, "Fatal - Janus failed to read: %s\n", filename.c_str());
            return JANUS_INVALID_MEDIA;
        }

        double frame_rate = video.get(CV_CAP_PROP_FPS);
        Mat frame;
        bool got_frame = video.read(frame);
        if (!got_frame)
            return JANUS_INVALID_MEDIA;

        media.width = frame.cols;
        media.height = frame.rows;
        media.color_space = frame.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8;

        do {
            janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
            memcpy(data, frame.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
            media.data.push_back(data);
        } while (video.read(frame));
        return JANUS_SUCCESS;
    }

    media.width = img.cols;
    media.height = img.rows;
    media.color_space = (img.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8);

    janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
    memcpy(data, img.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
    media.data.push_back(data);

    return JANUS_SUCCESS;
}

int main(int argc, char**argv){

    if (argc != 2){
        std::cout << "invoke like this: ./video_create_templates <output directory>" << std::endl;
        exit(0);
    }

    janus_initialize("/scratch1/deliverables/fbi/umd/config/","/scratch1/","", 0);

    std::vector<janus_association> assocs;
    while(1){
        std::string s;
        std::cin >> s;
        if (0 == s.compare("go")){
            std::cout << "enter output template name"<< std::endl;
            std::cin >> s;
            janus_template tp;
            janus_create_template(assocs, ENROLLMENT_11, tp);
            ofstream f;
            std::ostringstream os;
            os << argv[1] << "/" <<  s << ".template";
            f.open(os.str().c_str());
            janus_serialize_template(tp, f);
            f.close();

            assocs.clear();

        }else{

            janus_media jm;
            janus_load_media(s, jm);
            std::vector<janus_track> tracks;
            janus_detect(jm, 20, tracks);
            if (tracks.size() != 1){
                std::cout << "found either 0 or more than 1 faces, can't use this image" << std::endl;
                continue;
            }
            janus_association ja;
            ja.metadata = tracks[0];
            ja.media = jm;
            assocs.push_back(ja);
        }
        
    }

    janus_finalize();


    return 0;

}
