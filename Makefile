all: trivial_harness video_create_templates create_templates perform_search perform_verification perform_clustering

trivial_harness: trivial_harness.o
	g++ -L../umd/lib/lib/ trivial_harness.o -o trivial_harness -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

video_create_templates: video_create_templates.o
	g++ -L../umd/lib/lib/ video_create_templates.o -o video_create_templates -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

create_templates: create_templates.o
	g++ -L../umd/lib/lib/ create_templates.o -o create_templates -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

perform_search: perform_search.o
	g++ -L../umd/lib/lib/ perform_search.o -o perform_search -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

perform_clustering: perform_clustering.o
	g++ -L../umd/lib/lib/ perform_clustering.o -o perform_clustering -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

perform_verification: perform_verification.o
	g++ -L../umd/lib/lib/ perform_verification.o -o perform_verification -lumd_janus -lopencv_shape -lopencv_stitching -lopencv_objdetect -lopencv_superres -lopencv_videostab -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_video -lopencv_photo -lopencv_ml -lopencv_imgproc -lopencv_flann -lopencv_core

%.o: %.cpp
	g++ -I../umd/lib/include/ -c $<

clean:
	rm -rf *.o trivial_harness video_create_templates create_templates perform_search perform_verification perform_clustering

