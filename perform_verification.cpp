#include "iarpa_janus.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>

#include <glob.h>
#include <vector>
#include <string>

using namespace cv;
using namespace std;


inline std::vector<std::string> glob(const std::string& pat){
    using namespace std;
    glob_t glob_result;
    glob(pat.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> ret;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        ret.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return ret;
}



janus_error janus_load_media(const string &filename, janus_media &media)
{
    Mat img = imread(filename);
    if (!img.data) { // Couldn't load as an image maybe it's a video
        VideoCapture video(filename, cv::CAP_FFMPEG);
        if (!video.isOpened()) {
            fprintf(stderr, "Fatal - Janus failed to read: %s\n", filename.c_str());
            return JANUS_INVALID_MEDIA;
        }

        double frame_rate = video.get(CV_CAP_PROP_FPS);
        Mat frame;
        bool got_frame = video.read(frame);
        if (!got_frame)
            return JANUS_INVALID_MEDIA;

        media.width = frame.cols;
        media.height = frame.rows;
        media.color_space = frame.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8;

        do {
            janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
            memcpy(data, frame.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
            media.data.push_back(data);
        } while (video.read(frame));
        return JANUS_SUCCESS;
    }

    media.width = img.cols;
    media.height = img.rows;
    media.color_space = (img.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8);

    janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
    memcpy(data, img.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
    media.data.push_back(data);

    return JANUS_SUCCESS;
}

int main(int argc, char**argv){

    if (argc != 1){
        std::cout << "invoke like this: ./perform_verification" << std::endl;
        exit(0);
    }

    janus_initialize("/scratch1/deliverables/fbi/umd/config/","/scratch1/","", 0);

    while(1){
        std::string s1;
        std::cin >> s1;
        janus_template t1;
        std::ifstream f1;
        f1.open(s1.c_str());
        janus_deserialize_template(t1, f1);
        f1.close();

        std::string s2;
        std::cin >> s2;
        janus_template t2;
        std::ifstream f2;
        f2.open(s2.c_str());
        janus_deserialize_template(t2, f2);
        f2.close();

        double sim;
        janus_verify(t1, t2, sim);

        std::cout << sim << std::endl;

    }



    janus_finalize();


    return 0;

}
