#include "iarpa_janus.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
using namespace std;

janus_error janus_load_media(const string &filename, janus_media &media)
{
    Mat img = imread(filename);
    if (!img.data) { // Couldn't load as an image maybe it's a video
        VideoCapture video(filename, cv::CAP_FFMPEG);
        if (!video.isOpened()) {
            fprintf(stderr, "Fatal - Janus failed to read: %s\n", filename.c_str());
            return JANUS_INVALID_MEDIA;
        }

        double frame_rate = video.get(CV_CAP_PROP_FPS);
        Mat frame;
        bool got_frame = video.read(frame);
        if (!got_frame)
            return JANUS_INVALID_MEDIA;

        media.width = frame.cols;
        media.height = frame.rows;
        media.color_space = frame.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8;

        do {
            janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
            memcpy(data, frame.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
            media.data.push_back(data);
        } while (video.read(frame));
        return JANUS_SUCCESS;
    }

    media.width = img.cols;
    media.height = img.rows;
    media.color_space = (img.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8);

    janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
    memcpy(data, img.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
    media.data.push_back(data);

    return JANUS_SUCCESS;
}

int main(int argc, char**argv){

    if (argc != 3){
        std::cout << "invoke like this: ./video_create_templates <video> <output directory>" << std::endl;
        exit(0);
    }

    janus_initialize("/scratch1/deliverables/fbi/umd/config/","/scratch1/","", 0);
    janus_template t;

    janus_media jm;
    janus_load_media(argv[1], jm);

	std::vector<janus_template> templates;
	std::vector<janus_track> tracks;
	janus_create_template(jm, ENROLLMENT_11, templates, tracks);

    for (std::size_t i=0; i<tracks.size(); i++){
        std::cout << "track " << i<< " track score: "<<tracks[i].detection_confidence << std::endl;
        for (std::size_t j=0; j< tracks[i].track.size(); j++){
            cv::Mat im(jm.height, jm.width, (jm.color_space == JANUS_BGR24 ? CV_8UC3 : CV_8UC3), jm.data[tracks[i].track[j].frame_number]);

            std::ostringstream os;
            os << "qqqq-person="<< i << "-" << j << "-conf="<< tracks[i].detection_confidence <<".png";

            if (tracks[i].track[j].face_x > 0 && tracks[i].track[j].face_x < jm.width && (tracks[i].track[j].face_x + tracks[i].track[j].face_width)> 0
                && (tracks[i].track[j].face_x + tracks[i].track[j].face_width) < jm.width &&  tracks[i].track[j].face_y> 0 && tracks[i].track[j].face_y < jm.height
                && (tracks[i].track[j].face_y+ tracks[i].track[j].face_height) > 0 && (tracks[i].track[j].face_y + tracks[i].track[j].face_height) < jm.height) {

                cv::imwrite(os.str(),
                            im(
                               cv::Rect(tracks[i].track[j].face_x, tracks[i].track[j].face_y, tracks[i].track[j].face_width,
                                       tracks[i].track[j].face_height)));
            }

        }
    }



    for (std::size_t i = 0; i < templates.size(); i++){
        ofstream f;
        std::ostringstream os;
        os << argv[2] << "/" <<  i << ".template";
        f.open(os.str().c_str());
        janus_serialize_template(templates[i], f);
        f.close();
    }

    std::cout << "wrote: "<< templates.size() << " templates to "<< argv[2]<< std::endl;
    janus_finalize();


    return 0;

}
