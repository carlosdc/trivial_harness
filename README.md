This is a harness to show that the UMD JANUS library can be used stand alone and
that it does not bake in foreign code i.e. the NOBLIS harness.

It has the fundamental benefit of not having layer upon layer of
complexification, very conceptual and easy to compile, run and audit. In its
simplest form, it requires 6 files (including 2 images):

* trivial\_harness.cpp
* Makefile
* obama.jpg
* obama2.jpg
* umd\_janus.so
* iarpa\_janus.h

There are new programs (September, 2017) that show usage of the entire JANUS
API.
