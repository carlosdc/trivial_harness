#include "iarpa_janus.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>

#include <glob.h>
#include <vector>
#include <string>

using namespace cv;
using namespace std;


inline std::vector<std::string> glob(const std::string& pat){
    using namespace std;
    glob_t glob_result;
    glob(pat.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> ret;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        ret.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return ret;
}



janus_error janus_load_media(const string &filename, janus_media &media)
{
    Mat img = imread(filename);
    if (!img.data) { // Couldn't load as an image maybe it's a video
        VideoCapture video(filename, cv::CAP_FFMPEG);
        if (!video.isOpened()) {
            fprintf(stderr, "Fatal - Janus failed to read: %s\n", filename.c_str());
            return JANUS_INVALID_MEDIA;
        }

        double frame_rate = video.get(CV_CAP_PROP_FPS);
        Mat frame;
        bool got_frame = video.read(frame);
        if (!got_frame)
            return JANUS_INVALID_MEDIA;

        media.width = frame.cols;
        media.height = frame.rows;
        media.color_space = frame.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8;

        do {
            janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
            memcpy(data, frame.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
            media.data.push_back(data);
        } while (video.read(frame));
        return JANUS_SUCCESS;
    }

    media.width = img.cols;
    media.height = img.rows;
    media.color_space = (img.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8);

    janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
    memcpy(data, img.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
    media.data.push_back(data);

    return JANUS_SUCCESS;
}

int main(int argc, char**argv){

    if (argc != 3){
        std::cout << "invoke like this: ./video_create_templates <gallery directory> <probe directory>" << std::endl;
        exit(0);
    }

    janus_initialize("/scratch1/deliverables/fbi/umd/config/","/scratch1/","", 0);
    std::vector<std::string> gallery_files = glob(std::string(argv[1]) + "/*.template");
    std::vector<janus_template> gal;
    std::vector<janus_template_id> ids;

    std::cout << "loaded gallery from directory" << std::endl;
    for (std::size_t i = 0; i < gallery_files.size(); i++){
        std::cout << gallery_files[i] << std::endl;
        janus_template t;
        std::ifstream f;
        f.open(gallery_files[i].c_str());
        janus_deserialize_template(t, f);
        f.close();
        gal.push_back(t);
        ids.push_back(i);

    }

    std::cout << std::endl;
    std::cout << std::endl;


    janus_gallery gallery;
    janus_create_gallery(gal, ids, gallery);


    std::vector<std::string> probe_files = glob(std::string(argv[2]) + "/*.template");

    for (std::size_t i = 0; i < probe_files.size(); i++){
        std::vector<janus_template_id> template_ids;
        std::vector<double> similarities;
        std::cout << probe_files[i] << std::endl;
        janus_template t;
        std::ifstream f;
        f.open(probe_files[i].c_str());
        janus_deserialize_template(t, f);
        f.close();
        janus_search(t, gallery, gal.size(), template_ids, similarities);
        std::cout << "results:"<<std::endl;
        for (std::size_t j = 0; j < gal.size(); j++){
            std::cout<< "position "<< j << ": " << gallery_files[template_ids[j]] << " with score: " << similarities[j]<< std::endl;
        }

    }


    janus_finalize();


    return 0;

}
