#include "iarpa_janus.h"

#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
using namespace cv;
using namespace std;

janus_error janus_load_media(const string &filename, janus_media &media)
{
    Mat img = imread(filename);
    if (!img.data) { // Couldn't load as an image maybe it's a video
        VideoCapture video(filename);
        if (!video.isOpened()) {
            fprintf(stderr, "Fatal - Janus failed to read: %s\n", filename.c_str());
            return JANUS_INVALID_MEDIA;
        }

        Mat frame;
        bool got_frame = video.read(frame);
        if (!got_frame)
            return JANUS_INVALID_MEDIA;

        media.width = frame.cols;
        media.height = frame.rows;
        media.color_space = frame.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8;

        do {
            janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
            memcpy(data, frame.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
            media.data.push_back(data);
        } while (video.read(frame));
    }

    media.width = img.cols;
    media.height = img.rows;
    media.color_space = (img.channels() == 3 ? JANUS_BGR24 : JANUS_GRAY8);

    janus_data *data = new janus_data[media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1)];
    memcpy(data, img.data, media.width * media.height * (media.color_space == JANUS_BGR24 ? 3 : 1));
    media.data.push_back(data);

    return JANUS_SUCCESS;
}

int main(int argc, char**argv){
    janus_initialize("/scratch1/deliverables/fbi/umd/config/","/scratch1/","", 1);
    janus_template t;

    janus_media jm;
    janus_load_media("obama.jpg", jm);
    janus_attributes ja;
    ja.face_x = 443;
    ja.face_y = 113;
    ja.face_width = 150;
    ja.face_height = 150;
    janus_track jt;
    jt.track.push_back(ja);
    janus_association jas;
    jas.media = jm;
    jas.metadata = jt;


    janus_media jm2;
    janus_load_media("obama2.jpg", jm2);
    janus_attributes ja2;
    ja2.face_x = 226;
    ja2.face_y = 36;
    ja2.face_width = 130;
    ja2.face_height = 130;
    janus_track jt2;
    jt2.track.push_back(ja2);
    janus_association jas2;
    jas2.media = jm2;
    jas2.metadata = jt2;




    std::vector<janus_association> jasvec;
    jasvec.push_back(jas);
    jasvec.push_back(jas2);
    janus_create_template(jasvec, ENROLLMENT_11, t);
    ofstream f;
    f.open("obama.template");
    janus_serialize_template(t,f );
    f.close();
    janus_finalize();


    return 0;

}
